#pragma once
class Calculator
{
public:
	void InputData();
	void OutputData();
	bool isCheckEnd();

	void Multiply();
	void Plus();
	void Minus();
	void Divide();

	void (Calculator::*Calc)();
	void CallFunc(void) { (this->*Calc)(); }

public:
	Calculator(int, int);
	Calculator();
	virtual ~Calculator();

private:
	int _one;
	int _two;
	char _op;
	int _result;
	int _count;
};

