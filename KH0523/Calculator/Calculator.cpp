﻿#include "Calculator.h"

#include <iostream>

Calculator::Calculator()
: _one(1)
, _two(1)
, _result(0)
, _count(0)
{
}

Calculator::Calculator(int one, int two)
{
	_one = one;
	_two = two;
}

Calculator::~Calculator()
{
}

void Calculator::InputData()
{
	std::cout << "수식 입력: ";//"두 개의 숫자 입력: ";
	std::cin >> _one;
	std::cin >> _op;
	std::cin >> _two;

	switch (_op)
	{
		case '*': {
			Calc = &Calculator::Multiply;
			break;
		}

		case '+': {
			Calc = &Calculator::Plus;
			break;
		}

		case '-': {
			Calc = &Calculator::Minus;
			break;
		}

		case '/': {
			Calc = &Calculator::Divide;
			break;
		}

		default: {
			std::cout << "It's invalid operator !" << std::endl;
			break;
		}
	}

	CallFunc();
}

void Calculator::Multiply()
{
	_result = _one * _two;
	_count++;
}

void Calculator::Plus()
{
	_result = _one + _two;
	_count++;
}

void Calculator::Minus()
{
	_result = _one - _two;
	_count++;
}

void Calculator::Divide()
{
	if (_two == 0)
	{
		std::cout << "Can't divide 0 !" << std::endl;
	}

	_result = _one / _two;
}

void Calculator::OutputData()
{
	this->CallFunc();

	std::cout << "연산 결과: " << _result << std::endl;
}

bool Calculator::isCheckEnd()
{
	bool rv = false;

	if (_one == 0 && _two == 0)
	{
		std::cout << "총 " << _count << " 번 연산하였습니다." << std::endl;
		rv = true;
	}

	return rv;
}
