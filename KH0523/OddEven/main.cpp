#include <iostream>

#define INPUT_SIZE 10

// Ȧ¦ �����ϱ�

void InputData(int* temp, int* even, int* odd, int evenIndex, int oddIndex);
void OutputData(int* even, int* odd);

int main()
{
	int temp[INPUT_SIZE] = { 0 };

	int even[5] = { 0 };
	int odd[5] = { 0 };

	int evenIndex = 0;
	int oddIndex = 0;

	InputData(temp, even, odd, evenIndex, oddIndex);
	OutputData(even, odd);

	system("pause");
}

void InputData(int* temp, int* even, int* odd, int evenIndex, int oddIndex)
{
	for (int i = 0; i < 10; i++)
	{
		std::cin >> temp[i];

		if (temp[i] % 2 == 0) // ¦��
		{
			even[evenIndex] = temp[i];
			evenIndex++;
		}
		else if (temp[i] % 2 == 1) // Ȧ��
		{
			odd[oddIndex] = temp[i];
			oddIndex++;
		}
	}
}

void OutputData(int* even, int* odd)
{
	std::cout << "¦��: ";
	for (int i = 0; i < 5; i++)
	{
		std::cout << even[i] << " ";
	}

	std::cout << std::endl;

	std::cout << "Ȧ��: ";
	for (int i = 0; i < 5; i++)
	{
		std::cout << odd[i] << " ";
	}

	std::cout << std::endl;
}