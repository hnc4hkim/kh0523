#pragma once

class Hero {

public:
	int Attack(); // 공격 수치 리턴
	int Defense(); // 방어 수치 리턴

private:
	char _name[16];
	char _type[16];
	char _id[16];
	int _level;
	int _hp;
	int _mp;

public:
	Hero(char* name, char* type, char* id, int level, int hp, int mp);
	Hero(const Hero& hero);
	~Hero();
	
	void ShowData();
};