#include <iostream>

int main()
{
	int counter = 0; // 카운터를 리셋

	counter++; // 버튼을 한번 누름
	counter++; // 버튼을 한번 누름

	std::cout << counter << std::endl; // 카운터 값을 출력

	// Counter ct;
	// ct.Reset();
	// ct.Click();
	// ct.Click();
	// ct.GetCount();

	// Counter* pCounter = (Counter* )malloc(sizeof(Counter));
	// free(pCounter);

	// Counter* pCounter = new Counter[5];
	// delete [] pCounter;

	system("pause");

	return 0;
}