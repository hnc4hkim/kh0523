#include <iostream>

#include "Point.h"

int main()
{
	int randomArray[6] = { 1, 1, 2, 3, 4, 5 };

	int seed = rand();
	int rand = randomArray[(seed % 6)];


	Point p(10, 20);
	Point s(30, 40);

	p.Show();
	s.Show();

	++p;
	//p.operator++();
	p.Show();

	//s++;
	//s.operator++(1);
	s.operator++(5);
	s.Show();


	//Point t = p + s;
	Point t = p.operator+(s);
	
	t.Show();

	system("pause");

	return 0;
}

