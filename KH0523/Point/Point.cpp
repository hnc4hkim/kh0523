#include "Point.h"
#include <iostream>

Point::Point()
{
}

Point::Point(int x, int y)
{
	this->_x = x;
	this->_y = y;
}

void Point::Show()
{
	std::cout << "(" << this->_x << ", " << this->_y << ")" << std::endl;
}

Point Point::operator++()
{
	Point temp = *this;
	++_x, ++_y;
	
	return temp;
}

Point Point::operator++(int adder)
{
	Point temp = *this;

	this->_x += adder, this->_y += adder;

	return temp;
}

Point Point::operator+(Point p)
{
	Point temp = *this;

	temp._x += p._x;
	temp._y += p._y;

	return temp;
}

Point Point::operator-(Point p)
{
	Point temp = *this;

	temp._x -= p._x;
	temp._y -= p._y;

	return temp;
}