#include <cstdio>
#include <iostream>
#include <Windows.h>

struct Point {
	int x;
	int y;
};

void SetPosition(Point* pPoint, int x, int y);
void Move(Point* pPoint, int x, int y);
void GotoXY(int x, int y);
void Show(const Point* pPoint);

void update(int tick);

int main()
{
	Point p1, p2;

	SetPosition(&p1, 10, 20);
	SetPosition(&p2, 50, 60);

	update(1000000000);

	Move(&p1, 5, 0);
	Move(&p2, 0, 5);

	Show(&p1);
	Show(&p2);

	system("pause");

	return 0;
}

void SetPosition(Point* pPoint, int x, int y)
{
	pPoint->x = x;
	pPoint->y = y;

	GotoXY(pPoint->x, pPoint->y);
}

void Move(Point* pPoint, int x, int y)
{
	pPoint->x += x;
	pPoint->y += y;

	GotoXY(pPoint->x, pPoint->y);
}


void GotoXY(int x, int y)
{
	COORD pos = { x, y };
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);

	std::cout << '*' << std::endl;
}

void Show(const Point* pPoint)
{
	printf("(%d, %d)\n", pPoint->x, pPoint->y);
}

void update(int tick)
{
	while (tick > 0)
	{
		tick--;
	}
}