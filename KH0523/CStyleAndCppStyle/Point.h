#pragma once

class Point {

public:
	void SetPosition(int x, int y);
	void Move(int x, int y);
	void Show();

protected:
	void GotoXY(int x, int y);

public:
	Point();
	virtual ~Point();

private:
	int _x, _y;
};



