#pragma once

class MyArray {

public:
	bool SetData(int pos, int data);
	bool GetData(int pos, int& data);

private:
	int* _pData;
	int _maxSize;

public:
//private: // 왜 생성자는 public ?
	MyArray();
	MyArray(int size);
	MyArray(const MyArray& obj);

	virtual ~MyArray();
};

