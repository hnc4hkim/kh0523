#include <cstdio>
#include <iostream>

#include "MyArray.h"

int main()
{
	/*
	 * 1 메모리 할당
	 * 2 생성자 호출 */
	MyArray data(10);
	
	int val = 0;

	for (int i = 0; i <= 10; i++)
	{
		if (! data.SetData(i, i))
			std::cout << "데이터 저장 실패!" << std::endl;

		if (! data.GetData(i, val))
			std::cout << "데이터 읽기 실패!" << std::endl;
		else
			std::cout << "Data = " << val << std::endl;

	}

	//MyArray data; // 실행되는가?

	MyArray data1;
	MyArray data2(10);
	MyArray data3(data2);

	

	system("pause");

	/*
	 * 1 소멸자 호출
	 * 2 메모리 반환(해제) */

 	return 0;
}