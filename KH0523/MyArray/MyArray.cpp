#include "MyArray.h"

#include <iostream>

bool MyArray::SetData(int pos, int data)
{
	if (pos < 0 || pos >= _maxSize)
		return false;
	
	_pData[pos] = data;

	return true;
}

bool MyArray::GetData(int pos, int& data)
{
	if (pos < 0 || pos >= _maxSize)
		return false;

	data = _pData[pos];

	return true;
}

/*
 * @ 생성자
 * 1 함수이다
 * 2 클래스의 이름과 같은 이름을 지닌다
 * 리턴하지도 않고, 리턴 타입도 선언되지 않는다 */

MyArray::MyArray()
: _maxSize(0)
{
	std::cout << "MyArray::MyArray() 생성자 호출" << std::endl;

	_maxSize = 100;
	_pData = new int[_maxSize];
}

MyArray::MyArray(int size)
{
	std::cout << "MyArray::MyArray(int size) 생성자 호출" << std::endl;

	_maxSize = size;
	_pData = new int[_maxSize];
}

MyArray::MyArray(const MyArray& obj)
{
	std::cout << "MyAray::MyArray(const MyArray& obj) 생성자 호출" << std::endl;

}

/*
 * @ 소멸자
 * 1 함수이다
 * 2 클래스의 이름 앞에 ~가 붙는 형태의 이름을 지닌다
 * 3 리턴하지도 않고, 리턴 타입도 선언되지 않는다
 * 4 매개 변수를 받을 수 없다. 따라서 오버로딩도 불가능하고, 디폴트 매개 변수 선언도 불가능하다 */

MyArray::~MyArray()
{
	std::cout << "MyArray::~MyArray() 소멸자 호출" << std::endl;

	delete [] _pData;
}