#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif // _MSC_VER

#define MAX_INPUT 128

#include <stdio.h>
#include <iostream>

int OpenAndCloseFileStream();
int OpenAndCloseFileStreamToSafe();
int SaveStudentScore();
int LoadStudentScore();

int main()
{
	LoadStudentScore();

	return 0;
}

int OpenAndCloseFileStream()
{
	FILE* fp;

	if ((fp = fopen("text.txt", "wt")) == NULL) // 파일 열기
	{
		printf("파일 열기에 실패했습니다.\n");
		return -1;
	}

	// 여기서 파일 읽기, 쓰기
	fputs("성적처리 결과\n", fp);

	fclose(fp);

	system("pause");

	return 0;
}

int OpenAndCloseFileStreamToSafe()
{
	errno_t err = 0;
	FILE* fp = NULL;

	if ((err = fopen_s(&fp, "textToSafe.txt", "wt")) != 0)
	{
		puts("파일 열기에 실패했습니다.\n");
		return -1;
	}

	// 여기서 파일 읽기, 쓰기
	fputc('A', fp);
	fputc('B', fp);
	fputs("내 이름은 김호!\n", fp);
	fputs("너의 이름은?\n", fp);
	fflush(stdout);
	
	fclose(fp);

	system("pause");

	return 0;
}

int SaveStudentScore()
{
	int students = 0;
	int sum = 0;
	int score[] = { 85, 90, 95, 70, 82, 60, 92, 88 };
	double average = 0.0;
	FILE* fp = NULL;

	students = sizeof(score) / sizeof(int); // 학생 수 계산
	for (int i = 0; i < students; i++) // 학생 수만큼 루프
	{
		sum += score[i]; // 점수의 총합을 계산
	}

	average = (double)sum / students; // 평균 계산

	if ((fp = fopen("student_score.bin", "wb")) == NULL)
	{
		printf("파일을 열지 못했습니다.\n");
		return -1;
	}

	//fputs("성적처리 결과\n", fp);
	//fprintf(fp, "총점: %d\n", sum);
	//fprintf_s(fp, "평균: %0.2f\n", average);

	fwrite("성적처리 결과", 14, 1, fp);
	fwrite("총점: ", 7, 1, fp);
	fwrite(&sum, sizeof(int), 1, fp);
	fwrite("평균: ", 7, 1, fp);
	fwrite(&average, sizeof(double), 1, fp);

	fclose(fp);

	system("pause");

	return 0;
}

int LoadStudentScore()
{
	FILE* fp = NULL;
	char text[MAX_INPUT] = { " " };

	int total = 0;
	double average = 0.0;

	if ((fp = fopen("student_score.bin", "rb")) == NULL) // 파일 열기
	{
		printf("파일을 열지 못했습니다.\n");
		return -1;
	}

	fread(text, 14, 1, fp);
	printf("%s", text);	

	fread(text, 7, 1, fp);
	fread(&total, sizeof(int), 1, fp);
	printf("%s %d", text, total);

	fread(text, 7, 1, fp);
	fread(&average, sizeof(double), 1, fp);
	printf("%s %0.2f", text, average);

	fclose(fp);

	system("pause");

	return 0;
}